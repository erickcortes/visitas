const { ObjectId } = require('mongodb');
const Visita = require('../models/visitas');
module.exports = (app, next) => {
  app.get('/visita', (req, resp) => {
    const query = req.query ? req.query : {};
    query.user = { $ne: [] }
    query.client = { $ne: [] }
    Visita.aggregate([
      {
        $lookup: {
          from: 'users',
          localField: 'supervisor',
          foreignField: '_id',
          as: 'supervisor'
        }
      },
      {
        $lookup: {
          from: 'users',
          localField: 'tecnico',
          foreignField: '_id',
          as: 'tecnico'
        }
      },
      {
        $lookup: {
          from: 'clients',
          localField: 'client',
          foreignField: '_id',
          as: 'client'
        }
      },
      {
        $match: query
      }
    ]).then(r => {
      resp.json(r);
    }, (error) => {
      resp.status(404).send(error);
    }).catch(next);
  });

  app.post('/visita', (req, resp) => {
    Visita.create(req.body)
      .then(user => {
        resp.json(user);
      }, (error) => {
        resp.status(404).send(error);
      }
      ).catch(next);
  });
  app.put('/visita/:id', (req, resp) => {
    const body = req.body;
    const _id = req.params.id;
    Visita.updateOne({ _id }, { $set: body })
      .then(user => {
        console.log(user)
        resp.json(user);
      }, (error) => {
        resp.status(404).send(error);
      }
      ).catch(next);
  });
  app.delete('/visita/:id', (req, resp) => {
    const _id = req.params.id;
    Visita.deleteOne({ _id })
      .then(user => {
        resp.json(user);
      }, (error) => {
        resp.status(404).send(error);
      }).catch(next);
  });
  return next();
};